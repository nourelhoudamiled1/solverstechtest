# SolversTechTest
## Getting started
1. Clone the repository to your local machine.
2. Run composer install to install the required packages.
3. Update the .env file with your database ( for me I named "solvers_tech").
4. Run the command php artisan migrate to migrate the tables
5. Run the command php artisan serve to start the server.
6. Call the endpoint /api/articles/import?siteRssUrl={siteRssUrl} ( replace {siteRssUrl}  with https://www.lemonde.fr/rss/une.xml OR https://www.theguardian.com/world/europe-news/rss for testing)  via POST method to import the articles.
7. Call the endpoint /api/articles to display the imported articles.
 Remarques : this endpoint returns the word with the most vowels in the 'WordWithMaxVowels'  for each article.
