<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = [
        'externalId',
        'importDate',
        'title',
        'description',
        'publicationDate',
        'link',
        'mainPicture'

    ];

}
