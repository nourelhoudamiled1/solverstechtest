<?php

namespace App\Http\Controllers\Import;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Import;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class ImportController extends Controller
{
    /**
     * import articles from xml url and save to database.
     *
     * @return \Illuminate\Http\Response
     * @param  \Illuminate\Http\Request  $request

     */
    public function importArticles(Request $request)
    {
        //validation request
        $validator = Validator::make($request->all(), ['siteRssUrl' => 'required|url']);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        //download url
        $url = $request->input('siteRssUrl');
        $response = Http::get($url);

        //parse the xml
        $data = $response->body();
        $xml = simplexml_load_string($data);

        //save the imports table
        $import = Import::create([
            'rawContent' => $data,
            'importDate' => now()
        ]);
        $import->save();

        //save the articles table + count what article been updated and created
        $countCreate = 0;
        $countUpdate = 0;
        foreach ($xml->channel->item as $item) {
            $externalId = (string) $item->guid;
            $importDate = now();
            $title = (string) $item->title;
            $description = (string) $item->description;
            $link = (string) $item->link;
            $mainPicture = (string) optional($item->children('media', true)->content)->attributes()['url'];
            $publicationDate = date('Y-m-d H:i:s', strtotime($item->pubDate));

            //update article if existe
            try {
                $article = Article::where('externalId', $externalId)->firstOrFail();
                //If article exists, Update the existing one
                $article->title = $title;
                $article->description = $description;
                $article->pubictionDate = $publicationDate;
                $article->link = $link;
                $article->mainPicture = $mainPicture;
                $article->importDate = $importDate;
                $article->save();
                $countUpdate++;
            } catch (ModelNotFoundException $e) {
                //If article doesn't exist,create one
                $article = Article::create([
                    'externalId' =>  $externalId,
                    'title' =>  $title,
                    'description' =>  $description,
                    'publicationDate' =>  $publicationDate,
                    'link' =>  $link,
                    'mainPicture' =>  $mainPicture,
                    'importDate' =>  $importDate,
                ]);
                $article->save();
                $countCreate++;
            }
        }

        //return the count created article and updated
        return response()->json([
            'message' => [
                'created' => "$countCreate new articles imported successfully",
                'update' => "$countUpdate  articles updated successfully",
            ]
        ]);
    }
}
