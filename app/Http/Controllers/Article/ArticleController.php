<?php

namespace App\Http\Controllers\Article;

use App\Http\Controllers\Controller;
use App\Models\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     * Display a list of articles.
     *
     * @return \Illuminate\Http\Response
     * @param  \Illuminate\Http\Request  $request
     */
    public function index(Request $request)
    {
        //get all articles
        $articles = Article::all();

        //send additionally the word with the most vowels (a,e,i,o,u,y) in the title.
        //If two or more words have the same number of vowels, send the longest one

        $data = [];

        foreach ($articles as $article) {
            $title = $article->title;
            if ($title) {
                $words = str_word_count($title, 1);
                $maxVowels = 0;
                $maxWord = '';
                foreach ($words as $word) {
                    $countVowels = preg_match_all("/[aeiouy]/i", $word);
                    if ($countVowels > $maxVowels || ($countVowels == $maxVowels && strlen($word) > strlen($maxWord))) {
                        $maxVowels = $countVowels;
                        $maxWord = $word;
                    }
                }
            }
            $data[] = [
                'externalId' =>  $article->externalId,
                'WordWithMaxVowels' =>  $maxWord,
                'title' =>  $title,
                'description' =>  $article->description,
                'publicationDate' => $article->publicationDate,
                'link' =>  $article->link,
                'mainPicture' =>  $article->mainPicture,
                'importDate' =>  $article->importDate,
            ];
        }

        //return the list of articles
        return response()->json(["data" => $data]);
    }
}
