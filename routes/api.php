<?php

use App\Http\Controllers\Article\ArticleController;
use App\Http\Controllers\Import\ImportController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//import articles endpoint
Route::post('articles/import', [ImportController::class , 'importArticles']);
//get all articles endpoint
Route::get('articles', [ArticleController::class, 'index']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
